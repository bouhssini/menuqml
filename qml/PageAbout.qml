import QtQuick 2.12
import QtQuick.Controls 1.4


MyPage {
    id: pageSettings

    title: qsTr("نبذة")


    Label {
        anchors.centerIn: parent
        text: qsTr("About")
    }
}
