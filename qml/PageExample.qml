import QtQuick 2.12
import QtQuick.Controls 1.4


MyPage {
    id: pageExample

    title: qsTr("أمثلة")

    Column {
        anchors.centerIn: parent
        spacing: 30*app.dp
        width: parent.width

        Label {
            id: labelButtons
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Press any button")
        }

        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width*0.9
            text: qsTr("Button 1")
            onClicked: labelButtons.text = qsTr("Button1 pressed")
        }

        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width*0.9
            text: qsTr("Button 2")
            onClicked: labelButtons.text = qsTr("Button2 pressed")
        }
    }
}
