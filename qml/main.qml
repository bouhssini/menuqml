import QtQuick 2.12
import QtQuick.Window 2.12
import Qt.labs.settings 1.1
import QtQuick.Controls 1.4

import mydevice 1.0

Window {
    id: app
    visible: true
    width: 480
    height: 640

    title: qsTr("Hello World")


    property string appTitle: "Template"


    property real dp: device.dp
    MyDevice { id: device }

    QtObject {
        id: palette
        //http://www.materialpalette.com/indigo/yellow
        property color darkPrimary: "#2a2107"
        property color primary: "#705819"
        property color lightPrimary: "#706000"
        property color text: "#FFFFFF"
        property color accent: "#FFEB3B"
        property color primaryText: "#212121"
        property color secondaryText: "#727272"
        property color divider: "#B6B6B6"

        property color currentHighlightItem: "#dcdcdc"
    }


    property int orientationPortrait: 1
    property int orientationLandscape: 2
    property int orientation: width > height ? orientationLandscape : orientationPortrait
    onWidthChanged: {
        menuView.x = app.width
    }


    property alias currentPage: loader.source

    property int menuWidth: app.orientation == app.orientationLandscape ? 300*app.dp : app.width*0.85
    property int widthOfSeizure: 15*app.dp
    property bool menuIsShown: Math.abs(menuView.x) == (app.width - menuWidth)  ? true : false
    property real menuProgressOpening


    Rectangle {
        id: normalView
        x: 0
        y: 0
        width: parent.width
        height: parent.height
        color: "white"

        //*************************************************//
        Rectangle {
            id: menuBar
            z: 5
            anchors.top: parent.top
            anchors.topMargin: 0
            width: parent.width
            height: app.orientation == app.orientationLandscape ? 40*app.dp : 50*app.dp
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: palette.primary
                }

                GradientStop {
                    position: 1
                    color: palette.darkPrimary
                }
            }
            Rectangle {
                id: menuButton
                anchors.verticalCenter: parent.verticalCenter
                width: 1.2*height
                height: parent.height
                scale: maMenuBar.pressed ? 1.2 : 1
                color: "transparent"
                anchors.right: parent.right
                anchors.rightMargin: 0
                MenuIconLive {
                    id: menuBackIcon
                    smooth: false
                    enabled: false
                    scale: (parent.height/height)*0.65

                    anchors.centerIn: parent
                    value: menuProgressOpening
                }

                clip: true
            }
            Label {
                id: titleText
                anchors.verticalCenter: parent.verticalCenter
                text: appTitle
                anchors.right: menuButton.left
                anchors.rightMargin: 0
                font.pixelSize: 0.35*parent.height
                color: palette.text
            }

            MouseArea {
                id: maMenuBar
                anchors.fill: parent
                onClicked:   onMenu()
            }
        }
        Image {
            id: imgShadow
            anchors.top: menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 6*app.dp
            z: 4
            source: "qrc:/images/shadow_title.png"
        }


        //*************************************************//
        Rectangle {
            id: menuView
            anchors.top: menuBar.bottom
            height: parent.height - menuBar.height
            width: menuWidth
            z: 3
            MainMenu {
                id: mainMenu
                anchors.fill: parent
                onMenuItemClicked: {
                    onMenu()
                    loader.source = page
                }
            }
            x: app.width
              Behavior on x { NumberAnimation { duration: 300 } }
            //Behavior on x { NumberAnimation { duration: 100; easing.type: Easing.OutQuad } }
            onXChanged: {
                menuProgressOpening =( 1- Math.abs( (menuView.x - (app.width - menuWidth)) / menuWidth ))
            }

            MouseArea {
                width: app.menuIsShown ? (app.width - menuWidth) : widthOfSeizure
                height: parent.height
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: app.menuIsShown ? (menuWidth - app.width) : -widthOfSeizure


                drag {
                    target: menuView
                    axis: Drag.XAxis
                    minimumX: app.width-menuView.width
                    maximumX: app.width
                }
                onClicked: {
                    if(app.menuIsShown) app.onMenu()
                }
                onReleased: {

                    if ( (menuView.x - (app.width - menuWidth)) > menuWidth*0.5  ) {
                        menuView.x = app.width //close the menu
                    } else {
                        menuView.x = app.width - menuWidth //fully opened
                    }
                }
            }
        }
        Image {
            id: imgShadowMenu
            anchors.top: menuBar.bottom
            anchors.bottom: menuView.bottom
            width: 6*app.dp
            rotation: 180
            anchors.right: menuView.left
            anchors.rightMargin: 0
            z: 5
            source: "qrc:/images/shadow_long.png"
            visible: menuView.x != app.width
        }
        Rectangle {
            id: curtainMenu
            z: 1
            anchors.fill: parent
            color: "black"
            opacity: app.menuProgressOpening*0.5
        }


        //*************************************************//
        Loader {
            id: loader
            anchors.top: menuBar.bottom;
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            //asynchronous: true
            onStatusChanged: {
                if( status == Loader.Loading ) {
                    curtainLoading.visible = true
                    titleText.text = appTitle
                } else if( status == Loader.Ready ) {
                    curtainLoading.visible = false
                } else if( status == Loader.Error ) {
                    curtainLoading.visible = false
                }
            }
            onLoaded: {
                titleText.text = item.title
            }
            Rectangle {
                id: curtainLoading
                anchors.fill: parent
                visible: false
                color: "white"
                opacity: 0.8
                BusyIndicator {
                    anchors.centerIn: parent
                }
            }
        }
    }

    function onMenu() {
        menuView.x = app.menuIsShown ? app.width : app.width - menuWidth
    }

    Component.onCompleted: {
        currentPage = "PageExample.qml"
        mainMenu.currentItem = 0
    }
}


