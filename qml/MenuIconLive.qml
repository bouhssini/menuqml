import QtQuick 2.12

//based on https://gist.github.com/cyberbobs/8d62ab257d332914a72c

Item {
    id: root
    width: 50
    height: 50

    property real value: 0 //from 0 to 1

    rotation: root.state == "menu" ? value*180 : -value*180//-180

    Rectangle {
        id: barUp
        x: 5 + 7.5*value
        y: 11 + 3*value
        width: 36 - 7*value
        height: 4
        rotation: 45*value
        antialiasing: true
    }

    Rectangle {
        id: barMiddle
        x: 5 + value
        y: 23 + 2*value
        width: 36 - 3*value
        height: 4
        antialiasing: true
    }

    Rectangle {
        id: barDown
        x: 5 + 7.5*value
        y: 35 + value
        width: 36 - 7*value
        height: 4
        rotation: -45*value
        antialiasing: true
    }

    state: "menu"
    states: [
        State { name: "menu" },
        State { name: "back" }
    ]

    onValueChanged: {
        if(value == 1) root.state = "back"
        else if(value == 0) root.state = "menu"
    }

}
