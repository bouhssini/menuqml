import QtQuick 2.12
import QtQuick.Controls 2.12


MyPage {
    id: pageSettings

    title: qsTr("الإعدادات")


    Label {
        anchors.centerIn: parent
        text: qsTr("Settings")
    }
}
